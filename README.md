# Ribosomal MSAs for 42

`42` is routinely used in our labs as a stand-alone contamination detection tool
to spot foreign sequences in transcriptomic or genomic data. To this end, `42` 
comes with two sets of ribosomal protein MSAs: one set of 78 eukaryotic MSAs, 
manually curated and continuously enriched with new species in H. Philippe's 
lab, and one set of 90 prokaryotic MSAs, fetched from RiboDB 
(<https://umr5558-proka.univ-lyon1.fr/riboDB/ribodb.cgi>). They are provided 
with two sets of `queries` in the `queries-XXX.idl` files, along with two
`mapper-XXX.idm` files for `ref_orgs`. These files are required by `42` in order
to perform the enrichment. For more details please refer to `42`'s Manual
(<https://metacpan.org/release/Bio-MUST-Apps-FortyTwo>).

There is also a consolidated dataset designed to detect both eukaryotic and
prokaryotic ribosomal proteins at once. It is composed of 98 MSAs corresponding
to the controlled merger of the two individual datasets. Ribosomal proteins for
which multiple distinct paralogous genes do exist in eukaryotes (P1P2, eL8 and
eL24) are combined into a single MSA for each family, along with their
prokaryotic homologues, but sequence ids are tagged after the specific
eukaryotic copy. The details about how this consolidated dataset was built are
available in `docs` directory (see especially `README-life.txt`). This dataset
is still considered as experimental and its results should be taken with a grain
of salt. In particular, organelle ribosomal proteins can be flagged as unknown
contaminations, which is expected but misleading.

Finally, `-fast` variants of the `queries-XXX.idl` and `mapper-XXX.idm` files
are provided for rapid execution of `42`. Again, these are experimental.
