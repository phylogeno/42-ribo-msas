# log for creation of consolided life files
# actual data is nearly identical to originally released files

cd /media/vol1/databases/
cd 42-ribo-msas/
cd MSAs/

# control taxonomic composition of prokaryotic (RiboDB dataset)
cd prokaryotes/
perl -i -nle 'tr/;//d; print' *ali
for f in *.ali; do grep \> $f | cut -c2- > `basename $f .ali`.idl; done
fetch-tax.pl --taxdir=/media/vol1/databases/taxdump-20210216/ --missing=MISSING *.idl 2> prokaryotes.taxlog
cut -f4 *tax | cut -f2 -d';' | sort | uniq -c > prokaryotes.taxcount
cat prokaryotes.taxcount
#     12888  Archaea
#    197063  Bacteria
ls *.ali | cut -f1 -d'.' | sort -V > prokaryotes.list
cd ../

# control taxonomic composition of eukaryotic (Hervé's dataset)
cd eukaryotes/
perl -i -nle 's/Manchomonas/Amastigomonas/; print' *ali
for f in *.ali; do grep \> $f | cut -c2- > `basename $f .ali`.idl; done
fetch-tax.pl --taxdir=/media/vol1/databases/taxdump-20210216/ --missing=MISSING *.idl 2> eukaryotes.taxlog
cut -f4 *tax | cut -f2 -d';' | sort | uniq -c > eukaryotes.taxcount
cat eukaryotes.taxcount
#   34108 Archaea
#      33 Bacteria
#       2 Baculoviridae
#  202077 Eukaryota
#    3217 MISSING
#      82 Riboviria
#      19 unclassified sequences
#       1 Varidnaviria

# check causes for missing lineages: nothing critical
grep -h MISSING *tax | grep -v \# | grep -vi uncultured | grep -vi candidat | grep -vi contam | grep -vi archae | cut -f1 -d'@' | sort | uniq -c
#       2 Bacterium tmed221
#       1 Bacterium tmed264
#      98 Ctenophora sp3
#       3 Diatomovora amoena
#       5 Food Favella_ehrenbergii
#      41 Nitrocosmicus oleophilus
#       1 Simplicia sp.
ls *.ali | cut -f1 -d'.' | sort -V > eukaryotes.list

# subsample eukaryotes only
cat > eukaryotes.idl
# +Eukaryota
tax-filter-ali.pl --taxdir=/media/vol1/databases/taxdump-20210216/ --filter=eukaryotes.idl --out=-euka *ali 
grep \> *-euka.ali | wc -l
#   202077

# tag paralogues
ls *-{A,B,C,D}-euka.ali | perl -nle '($class) = m/-([A-D])-/; print qq{perl -i -nle "s/^>/>$class-/; print" $_}' > tag-para-ali.sh
cat tag-para-ali.sh | sh
cd ../

# design data merge
mkdir life
cd life/
cat > life.list
# from Wikipedia (https://en.wikipedia.org/wiki/Ribosomal_protein) sorted using -V
vimdiff life.list ../prokaryotes/prokaryotes.list 
vimdiff life.list ../prokaryotes/eukaryotes.list 
vimdiff ../prokaryotes/prokaryotes.list ../eukaryotes/eukaryotes.list 
cd ../

# setup data merge
# idea: take prokaryote files as basis then add non-redudant eukaryotes files
# note: * allow for eukaryote paralogous files (A, B, C, D)
cd prokaryotes/
for f in *.ali; do cp $f ../life/`basename $f .ali`-prok.ali; done
cd ../life/
mv ../eukaryotes/*-euka.ali .
for f in *-prok.ali; do out=`basename $f -prok.ali`; echo "cat $f $out-*euka.ali > $out-life.ali"; done > merge-ali-prok.sh
for f in *-euka.ali; do out=`basename $f -euka.ali`; echo "cat $out-prok.ali $f > $out-life.ali"; done | egrep -ve "-[ABCD]-" > merge-ali-euka.sh 
perl -anle 'print unless $seen{$F[4]}++' merge-ali-prok.sh merge-ali-euka.sh > merge-ali-life.sh

# merge and degap data
# final format is unaligned .ali but this is very much like .fasta 
cat merge-ali-life.sh | sh
ali2fasta.pl --degap *.ali
rm -f *ali
fasta2ali.pl *fasta
rm -f *fasta
grep \> *-life.ali | wc -l
#  412028
grep \> *-prok.ali *-euka.ali | wc -l
#  412028
rm -f *-prok.ali *-euka.ali
rm -f merge-ali-prok.sh merge-ali-euka.sh
cd ../../

# setup queries
cd queries/
cp queries-prokaryotes.idl queries-life.idl
nano queries-life.idl
# for eukaryotes add:
# 	Acanthamoeba castellanii_Neff_1257118
# 	Guillardia theta_CCMP2712_905079
# 	Trypanosoma brucei_5691
# 	Cyanophora paradoxa_2762
# 	Emiliania huxleyi_CCMP370_2903
# 	Monosiga brevicollis_MX1_431895
# 	Ustilago maydis_5270
# 	Porphyridium purpureum_35688
# 	Stylonychia lemnae_5949
# 	Plasmodiophora brassicae_37360
# 	Ectocarpus siliculosus_2880
# 	Arabidopsis thaliana_3702
tail -n12 queries-life.idl | while read -r line; do grep "$line" ../MSAs/eukaryotes/*ali | cut -f1 -d'@' | sort | uniq | cut -f2 -d':' | sort | uniq -c; done
# 	78 >Acanthamoeba castellanii_Neff_1257118
# 	78 >Guillardia theta_CCMP2712_905079
# 	75 >Trypanosoma brucei_5691
# 	77 >Cyanophora paradoxa_2762
# 	78 >Emiliania huxleyi_CCMP370_2903
# 	78 >Monosiga brevicollis_MX1_431895
# 	76 >Ustilago maydis_5270
# 	78 >Porphyridium purpureum_35688
# 	74 >Stylonychia lemnae_5949
# 	75 >Plasmodiophora brassicae_37360
# 	78 >Ectocarpus siliculosus_2880
# 	78 >Arabidopsis thaliana_3702
 
# setup reference genomes
cd ../ref_orgs/
mkdir life
cd life/
for f in ../prokaryotes/*.fasta; do ln -s $f; done
ln -s ../eukaryotes/Polysphondylium_pallidum_13642_abbr_d99.faa 
ln -s ../eukaryotes/Leishmania_infantum_435258_abbr_d99.faa 
ln -s ../eukaryotes/Monosiga_brevicollis_81824_abbr_d99.faa 
ln -s ../eukaryotes/Ustilago_maydis_5270_abbr_d99.faa 
ln -s ../eukaryotes/Porphyridium_purpureum_35688_abbr_d99.faa 
ln -s ../eukaryotes/Stylonychia_lemnae_755200_abbr_d99.faa 
ln -s ../eukaryotes/Ectocarpus_siliculosus_2880_abbr_d99.faa 
ln -s ../eukaryotes/Arabidopsis_thaliana_3702_abbr_d99.faa 
for f in *.faa; do mv $f `basename $f .faa`.fasta; done
cat ../prokaryotes/mapper-prokaryotes.idm ../eukaryotes/mapper-eukaryotes.idm > mapper-life.idm
nano mapper-life.idm 
# 	for eukaryotes keep only:
# 	Arabidopsis thaliana_3702
# 	Ectocarpus siliculosus_2880
# 	Leishmania infantum_435258
# 	Monosiga brevicollis_81824
# 	Polysphondylium pallidum_13642
# 	Porphyridium purpureum_35688
# 	Stylonychia lemnae_755200
# 	Ustilago maydis_5270
cd ../

# arrange docs
mkdir docs
cd docs/
mv ../MSAs/prokaryotes/prokaryotes.* .
mv ../MSAs/eukaryotes/eukaryotes.* .
mv ../MSAs/eukaryotes/tag-para-ali.sh .
mv ../MSAs/life/merge-ali-life.sh .
nano README-life.txt
# this file
